PGDMP     &    
                z            postgresql_marketing_contacts    11.16    14.5 (Homebrew) 6    .           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            /           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            0           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            1           1262    16538    postgresql_marketing_contacts    DATABASE     r   CREATE DATABASE postgresql_marketing_contacts WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
 -   DROP DATABASE postgresql_marketing_contacts;
                postgres    false            2           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    3            X           1247    16540    email_event_type    TYPE     �   CREATE TYPE public.email_event_type AS ENUM (
    'Bounce',
    'Click',
    'Complaint',
    'Delivery',
    'Open',
    'Reject',
    'RenderingFailure',
    'Send'
);
 #   DROP TYPE public.email_event_type;
       public          postgres    false            [           1247    16558    text_message_event_type    TYPE     C   CREATE TYPE public.text_message_event_type AS ENUM (
    'Sent'
);
 *   DROP TYPE public.text_message_event_type;
       public          postgres    false            �            1259    16561    email_events    TABLE     �   CREATE TABLE public.email_events (
    id integer NOT NULL,
    timestamptz time with time zone NOT NULL,
    event_type public.email_event_type NOT NULL,
    batch_id text NOT NULL,
    ses_message_id text NOT NULL
);
     DROP TABLE public.email_events;
       public            postgres    false    600            �            1259    16567    email_events_id_seq    SEQUENCE     �   CREATE SEQUENCE public.email_events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.email_events_id_seq;
       public          postgres    false    196            3           0    0    email_events_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.email_events_id_seq OWNED BY public.email_events.id;
          public          postgres    false    197            �            1259    16569    email_templates    TABLE     �   CREATE TABLE public.email_templates (
    id integer NOT NULL,
    name text NOT NULL,
    template text,
    template_arguments json
);
 #   DROP TABLE public.email_templates;
       public            postgres    false            �            1259    16575    email_templates_id_seq    SEQUENCE     �   CREATE SEQUENCE public.email_templates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.email_templates_id_seq;
       public          postgres    false    198            4           0    0    email_templates_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.email_templates_id_seq OWNED BY public.email_templates.id;
          public          postgres    false    199            �            1259    16577    emails    TABLE     �   CREATE TABLE public.emails (
    id integer NOT NULL,
    timestamptz time with time zone NOT NULL,
    recipient text NOT NULL,
    email_template_id integer NOT NULL,
    batch_id text NOT NULL,
    ses_message_id text NOT NULL
);
    DROP TABLE public.emails;
       public            postgres    false            �            1259    16583    emails_id_seq    SEQUENCE     �   CREATE SEQUENCE public.emails_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.emails_id_seq;
       public          postgres    false    200            5           0    0    emails_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.emails_id_seq OWNED BY public.emails.id;
          public          postgres    false    201            �            1259    16585 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);
    DROP TABLE public.migrations;
       public            postgres    false            �            1259    16591    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public          postgres    false    202            6           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
          public          postgres    false    203            �            1259    16593    text_message_events    TABLE     �   CREATE TABLE public.text_message_events (
    id integer NOT NULL,
    timestamptz time with time zone,
    recipient_id integer NOT NULL,
    text_message_event_type public.text_message_event_type NOT NULL
);
 '   DROP TABLE public.text_message_events;
       public            postgres    false    603            �            1259    16596    text_message_events_id_seq    SEQUENCE     �   CREATE SEQUENCE public.text_message_events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.text_message_events_id_seq;
       public          postgres    false    204            7           0    0    text_message_events_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.text_message_events_id_seq OWNED BY public.text_message_events.id;
          public          postgres    false    205            �            1259    16598    users    TABLE     g  CREATE TABLE public.users (
    id integer NOT NULL,
    name text NOT NULL,
    first_name text,
    middle_name text,
    last_name text,
    nickname text,
    email text,
    failures integer DEFAULT 0,
    successes integer DEFAULT 0,
    subscribed boolean DEFAULT true,
    phone character varying(22),
    editsubscribedtoken character varying(64)
);
    DROP TABLE public.users;
       public            postgres    false            �            1259    16607    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    206            8           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    207            �           2604    24586    email_events id    DEFAULT     r   ALTER TABLE ONLY public.email_events ALTER COLUMN id SET DEFAULT nextval('public.email_events_id_seq'::regclass);
 >   ALTER TABLE public.email_events ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    197    196            �           2604    24587    email_templates id    DEFAULT     x   ALTER TABLE ONLY public.email_templates ALTER COLUMN id SET DEFAULT nextval('public.email_templates_id_seq'::regclass);
 A   ALTER TABLE public.email_templates ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    199    198            �           2604    24588 	   emails id    DEFAULT     f   ALTER TABLE ONLY public.emails ALTER COLUMN id SET DEFAULT nextval('public.emails_id_seq'::regclass);
 8   ALTER TABLE public.emails ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200            �           2604    24589    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202            �           2604    24590    text_message_events id    DEFAULT     �   ALTER TABLE ONLY public.text_message_events ALTER COLUMN id SET DEFAULT nextval('public.text_message_events_id_seq'::regclass);
 E   ALTER TABLE public.text_message_events ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204            �           2604    24591    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206                       0    16561    email_events 
   TABLE DATA           ]   COPY public.email_events (id, timestamptz, event_type, batch_id, ses_message_id) FROM stdin;
    public          postgres    false    196   >       "          0    16569    email_templates 
   TABLE DATA           Q   COPY public.email_templates (id, name, template, template_arguments) FROM stdin;
    public          postgres    false    198   �?       $          0    16577    emails 
   TABLE DATA           i   COPY public.emails (id, timestamptz, recipient, email_template_id, batch_id, ses_message_id) FROM stdin;
    public          postgres    false    200   I       &          0    16585 
   migrations 
   TABLE DATA           ;   COPY public.migrations (id, "timestamp", name) FROM stdin;
    public          postgres    false    202   `v       (          0    16593    text_message_events 
   TABLE DATA           e   COPY public.text_message_events (id, timestamptz, recipient_id, text_message_event_type) FROM stdin;
    public          postgres    false    204   vw       *          0    16598    users 
   TABLE DATA           �   COPY public.users (id, name, first_name, middle_name, last_name, nickname, email, failures, successes, subscribed, phone, editsubscribedtoken) FROM stdin;
    public          postgres    false    206   �z       9           0    0    email_events_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.email_events_id_seq', 115, true);
          public          postgres    false    197            :           0    0    email_templates_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.email_templates_id_seq', 1, true);
          public          postgres    false    199            ;           0    0    emails_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.emails_id_seq', 13, true);
          public          postgres    false    201            <           0    0    migrations_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.migrations_id_seq', 45, true);
          public          postgres    false    203            =           0    0    text_message_events_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.text_message_events_id_seq', 194, true);
          public          postgres    false    205            >           0    0    users_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.users_id_seq', 399, true);
          public          postgres    false    207            �           2606    16617 )   migrations PK_8c82d7f526340ab734260ea46be 
   CONSTRAINT     i   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT "PK_8c82d7f526340ab734260ea46be" PRIMARY KEY (id);
 U   ALTER TABLE ONLY public.migrations DROP CONSTRAINT "PK_8c82d7f526340ab734260ea46be";
       public            postgres    false    202            �           2606    16619    email_events email_events_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.email_events
    ADD CONSTRAINT email_events_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.email_events DROP CONSTRAINT email_events_pkey;
       public            postgres    false    196            �           2606    16621 (   email_templates email_templates_name_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT email_templates_name_key UNIQUE (name);
 R   ALTER TABLE ONLY public.email_templates DROP CONSTRAINT email_templates_name_key;
       public            postgres    false    198            �           2606    16623 $   email_templates email_templates_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT email_templates_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.email_templates DROP CONSTRAINT email_templates_pkey;
       public            postgres    false    198            �           2606    16625    emails emails_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.emails DROP CONSTRAINT emails_pkey;
       public            postgres    false    200            �           2606    16627 ,   text_message_events text_message_events_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.text_message_events
    ADD CONSTRAINT text_message_events_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.text_message_events DROP CONSTRAINT text_message_events_pkey;
       public            postgres    false    204            �           2606    16629 '   text_message_events unique_recipient_id 
   CONSTRAINT     j   ALTER TABLE ONLY public.text_message_events
    ADD CONSTRAINT unique_recipient_id UNIQUE (recipient_id);
 Q   ALTER TABLE ONLY public.text_message_events DROP CONSTRAINT unique_recipient_id;
       public            postgres    false    204            �           2606    16631    users users_email_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);
 ?   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_key;
       public            postgres    false    206            �           2606    16633    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    206            �           2606    16634    emails fk_email_template    FK CONSTRAINT     �   ALTER TABLE ONLY public.emails
    ADD CONSTRAINT fk_email_template FOREIGN KEY (email_template_id) REFERENCES public.email_templates(id) ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.emails DROP CONSTRAINT fk_email_template;
       public          postgres    false    198    200    3736            �           2606    16639    emails fk_recipient    FK CONSTRAINT     �   ALTER TABLE ONLY public.emails
    ADD CONSTRAINT fk_recipient FOREIGN KEY (recipient) REFERENCES public.users(email) ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.emails DROP CONSTRAINT fk_recipient;
       public          postgres    false    206    3746    200                �  x�����AE�ѢW�J;w��I�`0�q`��[;ɬ���aD����ܪ&,�Ir��`NU@=>���~������Ʈ6�^��F���k�R.�y��e��P�&�xP��B�c�U
N�������:��nW��ק���� BW��b����F4�>M[�n�j��Р���ʭ��7좊�A�کzr{�����6�� �����xOЗ���v-�$J�ܚ���٠��`�������?�������w>��|X��+��"l��)ע�4���� ��[�tOn�5��;�5����&B,{H~�,�y#hƏ����L�o��]���{0��k��<�����?�^�������}��n s�V3      "   _	  x��Z{s�H�;�m�V������1��XN���2�Tv�5�f#it�`����_�H�@�$���kW������{��G.uXp�3�р�x�p�s(��<9���W�I�>��=�~�Q�Љ��f�'��7��0I�xF��ɦ6������|��-gO�R#c�J�����i�[>f�:�#��S[��f̓�L3)=��k�o��;}������#�%��MfM�J-~8��6#��9$���q�3#՞�v�c�L}pe�;�6��ƀ<��F��u��:��&ϭ�	%�V'H���>r�#{=��f:�~�����72���|��'����nA�~�P�"o�k�E@1�` p�;��XlB�$ȃ � �h^��6N t5J����1�{��n�_8&+�5s�-#8e1�`��	 .H.m!>��z��Ro����4gu0$���=aC�Ww����x�6HӲm� �#��1��̵��r�	���,`H�%� �Ib�y��a<ʕ��fQTH=>0{e,:c|:�@�s)2�l�2=�_QlC��+\���"�bc�+�ƤL��Q,�>�M� ��G�	wp�Sw��qUe�K*R<�C}�f0��!�,p��1
x6�ʆ��Ywh�n��sڙ�ÝN=bޢ��Q%K��f5	�0w!�`����7�O hT�Z��P���UG/"���N����Y�^��:]�h���a�$����:�N��wtx��=C3�%�������E��\t��sb�β�#�/|�,ތ��J�$��H�?Iz�����n�}h����_�I���ȋk��(��P�xm Xl'�p`��$��` �2��M���=�$��b?�
S�>�V%�,x6�/��X����-ֵ�ɑ*L��v��v���ι�fm�A��[�jeI�gv����{�aduz��U��7�0B�"���zP�������(����*�6��1�Q/a�����|
A?f��+�1^.���q�^��rX~� �;U&>B�BG.�v�-r64�~oPFs)A8,�)��B�MCخK9o������H�Jm�DZ>xQ6�"G���Y�|�_�!յ��{�w)_X���j��7���J�zC��*%u�C�d�Ԅ�<֢�Uu�n���Y��i���]��sn��յ��h��B���$�7x��J��]*�4R>��q�݄�_#SA�T��ٷL�1�#-�=P7��l&���g��l"'�s�]��ǚ�@��O��8&�%-�6���R��'�ڝ+lIoz�K�<<$������VY	�J�M��1�������R�a/��bJ�#��t4�@78�*�5��&�4� ����������'��T2l�O�~�1����^I��/�HR�b�"G�zV�b]2�n�s)��s=� !���4yV����jѬJ�:���g�4�i4���-�@�	�1G$R1b�X����A��g���{9ǵ�ͥ3���Ѡ�����t3����l&5��Ԇg�ț�8C?��͛.M���Ou\>1RGoK�Q���u���*�ʌ[���[�ֺ��\�>%k��Ғ��-���2�ff,�-�%��l�g#��Z��=Sb����e;����li�i�:��-P`����Z�6j��.6W��	{�d[ ��g��88��������|�F
�YS�;��n�6VI��^N&�֋�c`V�7����#T�P|�bL��I��v�^(j��y]�ah>��Q�EF��NۘD��!�d�U�ԭ�y��ŕ��_�/_�i&_y���U�=�Tȷ�)��D,�f�N�Ώ��Q�CB	�qv�L�n�Bkd\�:��cv�/��I���G�ZB�!#u��#@���ّD}i��g�=���F+4��l��v6�v��߿����P�Y!��
��y5�E��K��)x���
�J�b���L���������(��U�vx���6T$q�	0�6[7�� ��B��_f�|J��36����*����xG� ������XmLsW嘰��B+Z*o���n���>��֧;�$�P $�"�d����r��o=�ٶ������w��=%�o�=(��Ι�����*f�K��n�VmCA�9�u�v�?�F�.9L�G.�d8h��ŨP��1�Xd�j{/����`��+�	9��r+��2~��s�e��"�wx���ߓ''5��l�r� r��>9Ư&���'#_,@�����'}�f�7���<:T�b?����kǟ4��Y;���j{��0n=�;\ƭV�Z�c~ \�1��)�r����%6Rʧ��$��8��+�Â ��U�X�T���~���H|�&���9�q���v�0.�Y��4� �!�bi���(�h��d1c~,X�{E_pDeP=�5�,����/��?�c?�      $      x��}k��ʑ��뿲H"ߏ�$�w�{�k��q�E>%uIb���o����Hj��6,�`���.U�NF�8'3"�~�p_��qF��/ߎ�CR������.��_�/�*%�s̈Ꙗͳ�C`:���D��s�B�E'+#�5yU�g�6��jY0��*[�\��\d|���i�c��:���1���C�L� �90_Cf*m�Ύ��Y�c%e��bX�p��,x�X��)�c�/��_����U���x�C8Er�|l�q�ՉY�Tyb6�P\����i<�$sU%�bdMYŴV�%��F��[s)����/\t�y�Ï�Tmj�������%b����(+�~��Ŕ��k*~�UX�jb�'M��Ը�A��a������?D�TNŅ�lxVJ'�<p#d����3�\�� ,3w	���Q]�9ֵaEU���@�A�����{�����f�%`ի�,��E<	#��</p�n*)�Y�X�Z9˲D2�%a4�V�D2�,���_��������ƀ5�Gm�S��.i��H1<�q�Ä�ŧF?9:��"Y
�&�=���M����IqZ�NZ�ԏ�d�jn����C�XC�,��EE\j�V8��UcAd�W��%�G,�I��K�*×Hؘ��X��c���k�"�6���2/�`:`ciolpOx�I�r�TƦ��3�duK`��%��IϣҚ����KZ�:�6�ק
!����D���>�甬�5�'< ����	�6��_���
gJa�լ$�.xԌGw�D1>��!�r����?�nĤ�`&:l!����Z��cƭT2�%���|Q����J�?8��w�󈢈}�����!8o�7���3�ģ���Z)/0���gd�5-T�4�1.�#̌GvRZ)����똚���!!=sN`�{���{-:�dIѨ5��/��i�B�Q�SC�L�4��M&��ed!�
&�"�0E^��(�W؜�~�m�B�YQ�^ց��ur�I�/
{�I�?��� `�z�rQ��T��'LI��\}��Q!#�:�/�ؑ<U���ɏ��֪�
�P��0�*@D�3b~pRDb�"1	�R&��L|ƄMeC�5&������0�� u���i���_�E*��u���#�d��}�C�ϱ��o�QW�V�l���%k�#\�\��1Zg��jk��fԮV��YƷ�
TU/�$�����n��x���˧��E��/�q�e����gRf��đ��J�k��\ЈMp\�	�%^�Ԯ�����߽�/#J���� ܰ�!{tQF/ ?R,�Y��Đ�3��Y�H?�h����-nDS�;�1u���O�Y���B#����.�jD#H(Ν��\J���(�6<�C���m��a뫆@Nm�6�9O�y��W<̥��P�]������>���eL�:fK�9{�
�G-Zf^ǚ S�,y�R�1���bRN;O��k��ew=�s��O��2����Hكř�kG*���i��Pv%;-�ܴP�z���x���}���!y8�ւ	�Y=;0�uDbYC#?�u���wH6f�t�v�~��S���͏/�x��*�D�5�0r�7�*
S&��B�ϰ@ځ�)�Om����:t��w���c
�2��H� ߂0}�4R+��+̰t'|�q}�K�~�?#�Fцl��YJ)��F�aƥ���Z��fD����͙Bj��:��l[+��TQ�0Xd=|�)����NWA�i�kJ�hL�����>��$��*62� +s�#�/��&�x��(nk�6[��] ��������.X�8�~)<k (�"���ƆsHv$D\�ϙ���\��e��y��a�o\� �x��-H��\��IRy�y)�����Vq�N$�i���p�C_���^���U�tB��V�6W�*xzxfPOЗΔ��eVT�7���v=���+�q���~}P-��&A�!�g�I��y�v�_D�hX-�=b�2cX'p�5�N�m}�0t@!��|`�O�:Ik�(�z��"F�B١>ɣv�\�UoC�9+�|��N��*��_���D���]������1�R9��K���-j>\�S����b�����e����_��
ȰH�	/�!sy[)`�l!:��;i�?��[������~w����1ǽ�rQ��Cp�`gR.6�+
7�a�$����+.�ra\��Hɷ��	�����,3	ڈ�w�*������i[�e�j�4ӷ��V��t���g��ye[|yh}V<����yDK�tI:�� B��L�B�S�v\���ވE��b�0$�n�St�����V�R�V�j��6�Lr�5�� y �*�4��H%P�X �-�W�`��zfn�;���#%���ӱv�����{`H�f��F �5Io`��o#����A�`��[�N!����	F)�򡞮5~;�N�x�H�ȼ���;N�����aك� [�Z�r֥g&��|�v@K�zFx/��
1���u�,�� ���VX���ا]�VP�k�&P�A��m���?�8_x�������Rrf�r�<AV�dYڢt����H7��c?X��o7XݶNAa	�sM����� R���C���B�/����~2��r;@{?�����g����,2	o�T�c26�f���E��ӚZ���L.y�Q��Q�F�c���T�R�px�O���	\%rt�,�$r`:�V���Sffr):�8�Ӂ�PO��JW�0�ö��O2
���GQ��YnK�+����Rv\�?�@��q,���[v$9"�ܯ/�7ES8��sc�� S6�@'8��LlԊ��ӯRO��%� 9����!ÉJ�5��B�x�tw�Q�/>���.UG6J���k>^������z��p�I��t��]��L���*Б4/�	/�Z��1+<�7cp��pOiw�o���K^�1FU	�	� A��'f�J�i�h�@��>Ӻԝ4��O�W�i�Y�ĊE�:)GW��Q���j_��3�<����VH��FN�������v�CO���)A��q\�t�ӌS��^LP}���f@��S�)]��x��~�������8|�2%,]�k-���Z�chOa]+9�����f�ti;0������qw}C�C(�����2$�`i��t��`AS�,�4��N~��Q�E[Y�b
B�_3��p|�݇z>���_�Ʉ�T���V�����t���Lm�������N[9٫�5����Ƨ�|9/P D`4>�D���jM�>��{�'%�����z�/�����(<:�Yl)<+|��pc���J�V���Ԋ�5x?!�@@�b��W��.|��N�"�MW�!��˽uʮׄv���i�>���^�r���Iw�@c�a��6��,��&�#�6
�;�MeA��/-r�I<#�[�;��G���O���-���!2���5d�%�o�"DQ�Ï\�����n	�{�^�7H�M�7S�P�CJK�|���PU���}[�J�����Jt� �l}�pCp��5�KyM@� e�!%S*q����%�7!�j�m�W4�0��G���^�!G�-OʞJ�@��2�3]Wr\�)�ysJ	��X �,� �8���I��HB�"���+� f��doF�)U�d[���s3=+�����t�����W�w����UD`��%Iؤ�G�H:����AY�es�����8W�����<��^�ڶ@E+����=b�MtYI�m���!�#�Z�������q��F���~��.�ϗAu@����*I��u�����b���C�,�f�V���cD���	{�':>����eK��-��)!~�P+I�nS%���
Oj�,�+� 4������\:�\bSq�M�s���Ho�?	ٓC1��tY��(i�����:���[�\_SMb�f��+�U���5D���R�6b�n!k�!�s�(�����������_��-	&���^!���������ٹd/v$A�O{�ї�/���e<�Ru<��PI����#��	���5�����zv���    j�.�6�����w�2@R�@E{�/ c�"\e����;���A���	<�F���������s/�'�\",$	��B�����-���e�N��0A��g[���Q�2J�*�rE�V�� �ƒ�p^��g�ּ���Sv�Kߧ��o��~�׷��$p�ORc�d�$H[������7�x2/W0��F�x9��To�?���l\��˺�U���ոdr���S����Sn?����\����H�� �o/[�Ʊ��⭂֘���LD�>����sY���+(j3�C�Α��r�MwS"3���;mpt�*	|>�9Q�cku=�3Gk�)��$���Pg�r�_�b��ē�0�4�J���[��-�p�:�v���r��k��/��Ԝ���;$,C5�3��l��&!�%����Zuƻ0]P^+����:^��x~��zDf}V�t��2���9��I�9���;?Ӵ֝�b��H��ؽݑ8v	��rj}�n�e��b�uЯ�ceYo+��e)1@Ƶ%�����x[cǇ����p�ec�o�nH�T��jn��'��2v��7����y����`���0����׸�������	2�!Y�֕.j�a�T�(���z��Z�ZX��vR���@�������6�8Z�<�\V����3���Lkܶ�]��\Qa%A[�䯗���������g�C��h6*V��V"�ek��-t��v/N�"�Qa�o�i��t����y��o�˾� kWR�8�*��u��L���(�c�+ P?���G�a_�}A�;m;��k����E�8hx0�&�J���Id���S�iX��w�V�����Wo����C��G�	�K�����%lE{���X��`�-�>M�[�R��b�$	�C+q�Б�ކ#"�����KVp+Y�F��H�P;�3�������"��&7�Dnx�!4�sx�����,+$rl��X�]�aaZ��u�2DY��ʯ�_aQm��r�a�� ��~:�T��-�AlP�Bi9��B�VS1G�ȇΚ"�B8�E����"���aE6 �y�T�o�����s�J����f:���J\�TsE&%־2|vE�$�,�t��C���G�_(�N�ed+���υ�$�x�%YD���',c��k<����N
�������v�W촡�{U|�6V�'�L +m��^�2\p�B�bg��ө�ҝ��ݷ8��tmx	"<��E�'�K��1R�W^�҂r�4�Q���N���L�,��P�Jh&�'�Wꧠ8�"B�Ѧ&H��7+��	H��N2�z����~�~[dS_˞������b\m?�/f(q�U����L�Fӽ���!�=�"����:���P�t/��W�J����,V$3iRqAL�	�͛z�?5���2(|?��,�ml&�ܵԳD�W�<j�m+(����I9����ߺo�m��b]Ȟ�l2�፴Ǘ�����0�@�2��޶p�0�SQB�O���e���a�g��<���H���	(�־@V8x�v�p_ɖ�ؽ�Ս�}�����&0Udܒ�b�f*}���
iDR�[���U�����D0��d��x>v�����z��H̨���9Gw�:�Zj��ul]�+1
�B�:�ޯ��ݍ��w�zz�,�*�l�UA�A����kG���fL�^���b!����ԋ�oT9��}_/�n�����y������m���X� ����kl!�
��YD�n��Q�|o���UTz���J�t�O���s�KN��^7�X(;t��3U	�}�����Ǘ����Qr�^P�Hk�dB�`�g��b�����ZR��\6�����|�>�����YL���}r|h��[j2�:S^�T��-pꟜ�8��8D������ܪ
�G�V��eIM��ӹ���%瓑O�l�$,���.5a���[�v,u�]�U�P�6(0Ru��9Q2�� �59�d�i��S���E7�z���m[�H��E�jA0$���,K����Z%)=��:����?O��q�׾a��,_���bL��A���u��T�� �aD�p���iR�=J����o�jp��e��fX��Ot�uǓ|�@�|fj�f��I�<�&G<-G���~��P�}c �H듨?�l��SQV�еQʈ_�b�O}�r�y&ݬ_�?���q�A�o�ߢ�0d��^fX5�w�4��M�A�i�O~�NJjPA�)����v ��V��S-�/�͐]�*�#CU�>��x*Bg�Gzńox\AV���dݹ%�����4�TH���
�; 䋬�Ү������"r�����J��O����I���D�<|���V�9a�t���*e��*��rT�62���o]��pt�
�\�R���O[D���*=���+g˅�]'�t�~��������9�l[x��T �%��j�uxI�]���W4`"�Փ����)	ˁM���^Y��a$��#�gDu�&�0�����^L��|�ڗ�x� ��%�Ș9g�Ԙjm,�� 	t}���������ءCX��-�Z�p���4w ��Ǌ_Ksy4d�\d4@%E��f֠Vb�d��Z-j���qw�o�0Ѯ�3�9G�֠�f�W�������zO"�L�:-��xtV��R�>G� �U������p4�
���ٻ$���L�Bt\X1f����!~@��?�m�*!��^��x�KJ����l��8U���t��WLNq7C��=ա�"e-�W�56�sf�zARM�E���^���z�j�k!!��&(Ho���p��T?_�J����)��&]�@�Ȭ�qk��&�iV�T-6�ޤ�1~�n[��E)VKSƨ� ֐`m6��1�IAf�?�q+K�7�c<���]�����x���z�_-�H�p���U#o@RT��C��Ղ�T�=������>���g�}}?o�~h[����i���[����st�@� �s��������Gӣ�f�e�uK�E�G�di!`ר�63�cf�gӊppq�"陳��Qn:�?�o�k�񶭜�������L�%r��<= ���'�虴�62����*2���n�p�C�����{�$��-7� �喱q�xk���rEd�$����;EӦJ��tӊ��&ը}��v	�T�k�
߽�Y���X�T�_����T!��~��(�9fW`#�B�Bd�h����H�y<c_p��W�Y���q�o��'zl��%<TP,Hx�ZX��8�n�ۆ#�L�?�:腷}�mx�K8�oo��u���X�n����a��o�n�剄)�X"�w����Bݡ\���������u�F��p��D���#��#x���6<=0���:��!���4��.z����'�Z�(% IhT�xg�`���3�n�_͜-��t�n*��vȴ�f���P�U�B��9*f������\m�Ê��t�ޟϟ���v�ȸ/��#�
�NU�,��[xi�X�S��Mקef���S�||Z����/uc���(F3�x���D�Ċ��h�&5�a��dei3�4��\�����"���'��Y����4��*��ՀDG��h�B��[7��+$��TKK��T�wG���j�A"Q�1�S�1Z=���^=�F���j��dM���艬���|�;��	�^��-����8���E��t�8�%�ا�>�W@���'Y�-ƫ[�σEfpBV�a���Gڇ���'|����������G{6ȰC��7����f4���F�L1D�,U�r'��LOa=��q��!�އJ&(^6�װ�v;�P���TY�g���ϫZ��&�:AJ��R��p:S��}�����{w���$d.]-�� VX�~�h��+b ��O�H�RD�IP��p�lK��{��z�o�㹿��=��}d*LB-Ao��r�$�
�� h��x:=2ań����[�;�t��� 6কoM0d�i�q��J�?&��%�� &������=����`�htat�i2|��TJeA�����t{��Y�w4qtj��}�˟!�6�F�3^[r�0��S�� 9�-���'�o��x˧Σ��zE�o��`c�.� )  ��c��4���J���J2E?�YxBMR=ވ��h;�ŗK��0lG�>gH|�m�9)����d�KH��x�W0
��]4����k����=V��9�P�\f:A�8|�D�|��Z�(�L؊Sˬ|dY*^=^v�>�CW/�+y��EE���c����*��LD5 �"=U�X�"
N;%�:�c�԰��*� ��8���t�@���h�:�2�+��?5�;s�"�OCQϷ��v��a��ۿ��h����X���q�n���ƬZ�K�"����Jv�G��H���mxn�@LR!?��>	��o@Q���U�ۙ��a�E����㐡n�Du�r4G��\��C<�M������<�:�b�2x���BUݙ&l}w귍����:��T "Uy|��ܨ Ru�/�UҺ����H�O�Z��p<o����@|�iP[�`hr ��f9��}��f��FC�����k,��H%x!�>Ȳ4T���4tQ��y*Vqj�7v�{>������5�1����4�Q%-u���� �D�l����qVt�&�3]7ޯǼT4��猄Ũ�
f�d�A"��֪"��XWTe�N�'�i2b�&�.��xz���v~�
�Z��U�h2�v<��t�.hS����zal�=]4�w���e4��(��Z�����ԧ^i����g'�܊&8������h^N��v!ۑ�1Do`��5���[xAc�DYh~'����ae[Kݚ�
5�� �\A�Kz��	�/��	I�5��z��|M�}��il��n���TK<�����W7��h5���H[z+�L�Zt�1~*��o�[�\mI %8l(�DB�i��Ѭ�����}n�X�d��X��#��bJ1���F�.+�'�X�iAy�#q���`^������M&8��F-t�^����8J��0K��be����N�PH#�R��
j�k�:<.;%�����f|��7[���^�A�.�@w���,�v~=O�3Wk�	lʩr�k��� �@�*2,�q�rf�ca<"Wd��F��6i�_yv�����Lֆ^��t>3�!���D�l�O�z+�x���Hڈ�n�M�#?�5�885z���#�7�n#/Aс���"��La�/���tTc"_'e?S��������lw�WD2�|z	����QW�8g�бu��53�N �-����mhL�
Sc�0N[C��`�2мL���!�
]*P_/m(��`�Ӫ��lt���	��⣮y�L'8)�PR��ْ��kAAY�1��"����L�T���x���6;�9x�FSj��ϒ������d�|����
�,ٔ*R�[}w��c}y��Y����XyN/���W� �dJ� ��X_fz��aAպ��#Wo��}��J�ڟ�]�d�
���F3���T"��a�����V�R��*����v�jJ��c����3�a�1���	�FL3}�q���Y�`8=��#+��ҙt�����Ɔ�m�ӜY��(��Č�k����V"lȓ�4;*�S1�� ���k��[�~?�ۮ�!İH����Ы��'8N\�. ��T�������o�	�ZE��㋒�zI�(tS�5���q���g$��_'�������=�ʷ~��붾Tgb��A��K�5�k(V(i�Z�C5�� ����˛=��m������8Sh*~H^�
�j��: �}�X¬�Vţ�Q�s|�)�8��ͷ��;��h[�!�5J����ˑ㹯�*}�#������ $_�ه��D��cz�Q�'�+2M3�FBD%c�N9�v}�G��.�TSCS�v���u�b/!��<�����A8Ӕ@A�C��
�[�R`}�ɣ�ޯ�C/D�" ��Ǹ���:�d��{����
��*$Հ���jeE�VDH.�N壇��h�,�&�b�+�j�R�t<]�Q�����"k�gb�䐘�M��o���c[ϐ�w��t��԰�c���,UÔuU��`��Y!�]?��R��z�a���mE���P�ͤX"��9��hB��N�Tt��j�2�|a�-o�Й��w��o�uxY�8ns*�R�z�7l|� c��Z]���,yǥyT7�x;����b�$�q���8Rq"���A�����s��<��+�G��s]���X�Pi��X��4�	��b�
m��e����^1��B<��q�Nq@޶xv��(�*�P��� �zQT���AC�hF�ҏ�����4�+y��2�7�|��.�U�u��?�� -��qf]�����z�!��$��q@M-����9��T��J�]��S�r؏/�4�%K���v}�1���Q�h����!v��������:u���JHAGԠluz� �BQ��d��
�,4#f�f������}�h����Q�W�r�`���z��s�L�DQ�z�
�_�O+���G��s*%�o���8��6T��ɕ��E�&K�$X�=�¶R�(Ϩ̌Jt4�|*'l��v���-���Q]�1��q�8��p2�9N�����+->9��D�{�'X"�(���M/�!Po� Иj����5���[�6�F��VdnF&;�x�:��wD7@�]�!n}ɘW�!���.f��N��R��Y�"����3.�߃��@GY�~��k���g��x�^�kH�b^�' �ڧ�+Ā ����N���kMc��Ԡ�-���4&	p*���s4i}Ә�3�S��֝�P����/��1�^��7�^�t�O/��
�P�	��1܂g&v������i�����>�DQ�V�c�7\��%#�n!��-��wkc��3�k���A��Mv�\h}��ˍ�������c�������}3ɖ�-*���R��W�.����[�é�o�xz��	�Q$S�e� �m��,�E��!*�����i��3��ku�W���.�Mۭ�l���IUEo��� 7�=�1{���	������j�P93:��{��xVBo�8^�i*#�
�����6j��')���x��
1�_iWP����З{�C>Ĵ�٧�5��$Q0h�WjA���`j��ESm[y���i���6��%?�m�A�d|=\��7��{��8++�<Ct�WP�� uz�}���ڑ�$'ˢ�j����g�j��RwM�U�un��3qk�i&%<���N����(��)��#>���&�S�a������4__�-�L��B��G��wH��[�q̇]|��]�����z��4�.�܁��*���БeeUx��0�7}*�"s���5��ޭ�/��i0EgYUPz���i����C��$�4R�w�����/ڂyP      &     x�}��N�0F��aL��]�;��rwn�L�H[mˍ�/pą�ɜ���
^��3mՂRQTm�s�x�!�(���Z�x�l?���0M��qD�UT�N3;�;����3�h \�����I;C�.�ZђK(�Ѕ���c��0:�[B6}�	�Y/5SFCidq�u�o����Z�M8����hH.�Z���|�)�W<V�IpU�VҨ�������#V��m�s�I�	�9I`S0\��]�Oc���&���sĎ��B�7��      (   A  x�E�;��0C�xf1.=��M8q:�SG������ӔP}|}�h���������N�0���:�\L�����ɥɴ�M×6k��0u������X�^���e����M���,/D��Z��㋪eT�jf4T�Ȇ�Y�P5��fU��}*Y�P)WQɪ�Qɪ�Jf��*Y%T�J�d�8+�����.��V	U�J��UBխ꨺UU����S�_�4��y�^T>	i��_R4��S�g�7^Oy�g;8��u�=Y����w͌��>�fV搦W����'�4��A�|7&�<A��[�'w�ZfLTˌ�j��j�.T˪�jY�Pm��m�B��Z��Uնj��V-T;a�j[�Qm�6��0Au�ڨ�UUn�Fu�ڨ�Uձj�:VmTǪ��XuP�Ϋ�c��U�3Zucq2Zu�Ug3Zuc�y��O1'`��z�%�끖�G=�������n^?�n`?��E��&8��e_�D�n���)N���8)�v�×�.r^����n��K�Q��y��j��J�i��y�J�y�$z�J��_�|�|%��WB�}�6"��|ɾx�}%��W��H~%��׽dͻ��}��J��ϓ�귲��
Ј�P*����P���~%��W��%�P"�h ���=��ό�Ќ�PJ������%�"����JOU�{�t�RE(�Q���}����(�@)��t�{h���C%(�Qt�ֽ�/R��R!E-(R�R"E1h߷2|����)�A)���&)�A����.)�A)�� �6)B���"t�k"��KQJ�-�TJQJ�=��J�(�*�H���"�������ޝ��      *      x����r$7�-���6/4 ��U%�Ժ��J�m=�f�p2�d&'/U����c~쬅�L����9-�"��DD ���Cw_�?��v�׺���:��3��^8�c]�n���v{�������?w:�c�T���>�"z�e���t_=���>�V׵f��q���c��c��gk���z�џ�A�QF��0z�J�>�������>�Wq��.nJ�˪�v����EϿ��{��{����ONA����,O�Ouzk�UJk��"t_�����vs���uS�� F6��-��Iz�'��kml�S��Đj�!/���]��_���j&�n�����RW_���ĠzgD��U�s���f�!�_H�z����_���y{"�n���]�ߤF�ޭ�)n_>j�Y��u��l�59�C0[�&5D��e!�i�~���v3��L4�f"n������닧$-|L���&��ѻދ1�N�a���X�BZ|�&.�cr�z7�}ct�k>j9{�\�.�}�Rcރ����e�G�(�(��BB��㮖����Ǻ�'5�]�O�Pw7�Ƽ6c�i�B�������;dt��q�rS?��/�������<������=&��qp�2#\��2��a�I��
�;,�TwH�������0�� ̵��9� ���t�����Л���5;��BA*�������nv�3�u��{�C;ԍJ^{�-��X J�;r)=֎�mć�KNf����ŧ��^�xw���9�x�tM��9�����������7RִP��1Ǻ�۾|�DBӑ�oԵ��N����d��0bɀOJx��E��
"~L�U�Z(��]�؍A�p�t��v'�W��sԺ�:N�>���(�X�Mb� ������>�6s����F���6>>���B7����2p��A������ji�0�l3&(t��˯�8���LlG6�~B�2���W0=��>�0�.V������������?�6�ۘ��������EmK	Н1`ޝ���)S�fQ�8�Š�w���q��j�/k���̀>����|�w�5�o������G��݅0��
�S֩�"�)f�F#��>��.ݽ[Ǉ������vjw�DL�M#nW���C�s����!f�XCK?*ۧA�'j-�b0�+������o;��ƙ���(}'Te�	�Mg<*X	��Tk0�&/l�����!n��_��J�L�RL>�qb����"�,c�e����4�^A\�-����a��;������]��{T��7	�I騺��h��:������[���/���꺉��?��,�4��~���`���y�A�X��-��~�Տ���Dw݉q��̌%0�%{9B�3���-
v�}��۱z[�Xh�.�zp��z�������| P>��7�f��O�dx��鳄�P=���.fp��ȅ�x�����}��͜�;��<�g��'��n��Oda�L���� 44�B���jS��?�����&�ˏ9�q�?��ޭr[ʗ8�x��z��(b�XYrT��GZ����������p��	�<�'��D_�"%`)VMO�>:#���w����А�g  �?��j���hO<h�W�.O������/�ljP
2���P�c��#���n���,�쾎�]����+1��h�:s�éyS�/�Ν�9��{7$H�Kk����@h��\��o�1?��D�Y3��7����w��+������*Qc��&L������PJ�b,:-�龮��$���D6���,3�䵯cvFǱ/�o�+ 3 �������P����
����[��Vםy/̛�_�{h�4j3����I*�g$*��#+{��u��XǔРO�VW����P���6uw��(�{W��bј/�&
��O�|t��:/˯���D�C�Չ~�Y����&Z� �b0W҈>
 i�P�ʅ�7�����~��6a83�=&�'V�}�mi��K˩����	�R��9�;YU�ɖ�ª���#PL=}؉~�����q� eƔ(�3  �K�B�������7�U����n6�����f����?���?@[a����-� cz�=�*��Ð���B"�6/�a�˟*��NI<�Lw���>����.��`���6AaTuO]��t�_�_X�}m�Y~��X�30��]��~I^�2[�	,��N0�^��1);.l�]m?6l��G���L�Q�jy��qE��Rt���1F%�L�K0(�.��=>/?<�]9�$��OTw�����\��v�0��:	��Q��ㅵ䱖��A����Y8���_�����q����ʛ2*F�[\`����A*rV ��T� 7t�q�����O�ݷf�����Z���1;�mF���Ns�s�^�0���G�|���;��L�ϝ���ck��}��]<8��}u�'�35@��0���2,�������"{��&E�=��
��L]��"�8���R��L����ȁ��ǅs��y�#��y����y���Q�B���&B��`́O��0Ů�����1.��>3Ё&�kk�u~���=��߮Zt����mA�^,����L�F	�����~�Xwu�~Ww0�3@23����R�U�6F�O��PiՂ���y8�9@g��,��?	���~{���h����wҷ���������5`#0>4
)�k �)D!\6*�E���y���ٳ�F�͌�ssf�+��,&���n0 ��w����j�ո���W������:�MH �]l��zu�E�O�;���'���db�c_Հծ�����h8R�@%K��}�y���ؑF`�TWOS�bz���O0�i�3'@GH�)#}���,��~��{^��V6�]w�������1'�5QT-��8B_J��"�u0u!���;>�;��F��s��nj�>��]��V /�.�\<�W�x�֥11"�1%�w?Fئ���s����ܕƸy�[Ĉ� ���C�����0� �r �{ ��*a��y�`��[��'���.l�pP+Ԯn�E���SC�b�e��an׫�>�
l͜/"[��`2�uu}�qpV8)�|���`3�������`����W�U]c�l����5F71:�28�`�N�j�&�� ��\W(���?O�������)���52�����巭�����t�b=���A�[�7����#���ݦ�f��>��g
s5�3��6>���2ʠ�(TF��'f���fa�Ц\a��Wy���C]�m�-/$�e����x����`E��⨹�e��k�P�8k(��9N���றn���?⚨�T��3���^�sI�`�L�}���o��o��l�n_��v_��ˎ�� ކ!uf�2���P�j���u�p"=�w[x���DQ�Md�']���?����D���ǎ���V�]�P����C
�!����}#h&
_չ�8�y[��vu�_A�]:�9K_ǂ9�����A��BR#�
������G,���G5�ۓ�y q�`S7���eDU8]m�P��`���ߏY�����N�9��v��/��+r3�]w�����W��>T���sB;��9���0n����o�va�{�	ZRУ$��>�qU�(�(���@����p}�c29_��sL��U~XM������!�}�O�[�!�z��X�(�[�e�*���AU����j��{8Kԗ���S}wf`�fΙu3sn�3�uޖ7O�C5��*b���j=<-��¨��q�% ���W��c&�3=3>�/����U@��W�mp�����D��,�����]]�x\�?m��z=�:�3����Ǚ�ju�m��3�<���`\r^��6��e�C�e�~���;�Μ�����}����(�zv�� ��`��o�����.��~ح ����A�
�{��ۇ��׮�����id��@�U@[+"\��CV�껉���n���ɡ+�ݙ;c(3`E���̊Ył��6�~��j�����3M    ��x����Z
�x\��k��������'��؂M��rc����,�i>5�!Ə�پ ��{�8����&F�l��ЂS4��a�݋��v���͟h��Ƹͻ�����.^��e�	pFV��A`5h,�ࡎ�`F�A����=�����u�o��O����n��---Hj�'g��*'��@�:k�ا�v��?~{��7�NL�LvO�oґ������6Nn���x1U��CG�A�-�*��\oˈ��5��u��	��;Q���v��q�K:A;��AB9��죁��t�
&x
�ay��q�����~��Eks����0�I�2�do(Ɂ�8���^b9��z@#c�y{8,���$| +5Q0���v�c������)	8�Cq&;+8�B�t�W���Lql��}��@�]͇{�{1B�[V���=�{��0$�1BN�L�߶��	'cx���{1l�?o>�������U0NC�/�߫!�4�AA�;t�m�_�ϟ��3	H,��<6��%ܘ��
S`B�m��H��2�i�5LZ��_0Rs�z�^�D�<a�`����ϗ�:c6q��1:�Q��e�j�5~���P��	(ۙh� �*���7R��u}���"J�N�J����
�8$��A������]��4wH�{�97�Ƹ��PS1VQ���%r�r4��I��E�u�.y���=�D��1pnRc�~\"L�0��YJ?j;Љ� �\
@�L�p�����߳���|���|bPSΜn}w7�o7�Rޜ���J�������BA���:��:�"e۾�����:�&
�Lғ�n���[�(
�!o�2Ǵ-�Z_U�{eU-t�4�� ����o�R��i�$N<�qʧ�q��x�w3
Ղy��1I3�~���ϼ�/�w�u\/����1���&�D�±���)��-���V�%���1�BWz�S2�GSa[���#�?����c`�^8��S�H�W��$}�ǧ1�# G�|T����s�B���N�_����.�tםp���8���9f!�j�x���#�Uru ��#����7�{����9�u�0���q�{�v*ڮ8�]�ĝ��3id��jj�{ �S��Lw/YO��'Z^͆�Va���l�}�2|(�!�BW�;�������~���_-��럿��	I�i��;.�Oϟv�
��^�e��6f�8�)�����]�B( /����e�+���3�wh]��
L���)�W��D��o}��"f�$�� }}����Gx�k�xt��n�����C<�8/-<(R�)A�+�#�fL3�"T!\&�}���w���~����M���P�o�D���_>Ea˔��!x_�4��ʨ!.b�F��@k���kTC� o���<Ν	@O�J�9c4����!1d����~x���}�J�t>}�bt��n���r�l5n��w)2�R�)w*o�w�k(0���ū�	���l�&���3�����tg�*V�t�8*q�Rւ Wu?�Cdv�/X#'�>:M�f"o�@]y���h���^d[^��6�ޜ�ߙ�xF�:A��?Hz4�4�d�g詅�(kq��Z(��z���Z��6s� I&vv/$��;�q��'��B���m����U�#��sW��E���D�A�[�&%�m �]�׸�+q]�(5�Q`����-���`�\�7��a�	� .�����+�	�l�H��~{�I�n�r�#�o7<fe{W���Z�[��v\���Ǐ�0Q-!���1O��'z ����{��#��C�������cҼ?�
�p���sk�E"��7���3���(�#NP{l���4g�+fft�X-S�����u}�0pA{���	T���j��^<���������D0�/�-���j�fF$�0TF��dX�2�'�8��#4�g�����U̾Q��?nu>n6������~{�GPP�̮7s�G�g�۫,�61Yg3~���skp����ۺ��w�J�ҠT��V�d�/PW*<���!� �Z w7K&S�Z��+�qw[+�I��0�h�׾b(�茴�ӡH<�Ѱ��7�ňG&p�7�<f�{�y<W�<�AW&�C��A
�L��V�s����u��Z���b�Dם�F���G� ��+�X��4S�M�2�%�d��Fn�)F޵�J�euW`�1�����+���YC����9�Ж��E��ҍ���,�
A�n&
o=�?��m����q״�*C��E��n�O���B�rr2�צ��j��n�iӚ<'�v�%���*$�����w���/��v�<�/��.Ʊ2j�������pkB�ٵ��t=�-�gL�)�8h��"7���rs�5����㞛����OTw y?<V,�PW� ����e�ry��K;�.�;˨4�~�����$e�}\��ٺ�:�T,��rO	�����1x��{�j��f�c�~�)��Fv��[�S�jl�b
.7^�R=wt��pj�I2��I�~�Jj �c؞��\�z�ڙ���􈘂,!�&&���m���X�@	P�Z,h����.����L���WgZ��|�z�IJ������S<t	��X<�+$@�3|�[���ب��~�p=|=�1=L�g�*[:���=��0������±�L��z�9��Uu("Z�ZG��WOH���}��S�/�Zܩ�=Ɖh���'���[��Q�2^�H���v6U���q�$�0��|q�~ۭ���_�w/���wh,�!{{��lw��O�)u�����J��A5�}`�q�*/=R�[���`t��z���?�f��� 2&�a������p���`�!��d<dRA�m8��b(&矂vg���ü�a��s*�8@��ϣ�Q)g����O�޻���~v��v�9��/Ym�̽)t��%�;OP1�@N�G�p'e�U�� #���5��W�i��܈�[E+�{fFk�<\&�E��b. ���f�6��m&�{Ɬ�~+���cF�X��c/
�J3Z�G��t���|��x��\�&�f�v���~�GaM�J%a޽�a�C�Rױ~�)�D�d.�:
!o���K)v���qr��B�>�xՠ����e�ʹ���ڶFO{CdLۣm{�5n�+��n�f�E�?�!:\Wcx���*���4�w����X�:���iR�%a�v���1[�
��;����mw�}��ȳ=s���Լ�7�i����qAT`{�=�&�5-��(&�!����_�|��{^}���n��H��4���x�+�Y��gT,Ք+�m�Ua4x�m��U�������.����=bI�HG_�©�JFe���r�O���`BOO8�3NL�o�۫��B������w,a����F�L�se���۷;,b�Ss{�ם��>��1�Z���_<�( �2��,�h�?���s�j²g�f�~&��ƀq�}�5��(V�ҹ�T�'ih}�BG+X҇�C3�4�3�SÏ��1?<��M_`�r�f�t�?�?]�K &�s�����/<b��n+�D�ꩵo��U�����-��*�J �=,L�u��� �����<��L@�7����I�f�Zz��i��\x���5�1��B������mu��'�Z,�� �j��}�W����Wݧ��]���B%6 �4�����K!��J+����M���:���Lqo���1�4��M��U#��R��U���<�F������o����w��RO$$p���qs���NY3B�q#'S��ȝ}|��v`^pU\?���^K�r�5bbӶ�S�
����1��`x8y���V��Ǚ���.?�%��V��adpk�r�#����jҐ����+�yl��au�cS�K)f�����*�{���Mx�pw<Q�0E�ٶ� �0�m:S�y��8�-���=ƫ^&��2�>��t6FK@��ьF%/�Ó������	�3�ҩ�ۯ�
W���3]�ά&(��ه�$�U    k�Rd�DGu�p�֫<���Ľ�������6~�U�#Ct�5,(����~�����[(��L؋O�)���>�PD��7�o��+�L�W��*��'���}UF��]c�&<W�����Ɖ�ڙ���h)o�:��XACC�NT0���k'Ks����d�C��~�s�S2�3�Z�#�6�u�X�]l�*�x�?���W��K�
,[F0XMq�0x�;,���j�$��9hr� ��c,O�j,(3U��������B}����vw������������2Bc��δ!�Nhi�������e�aс9�d���ĸ��p��X�7r�+�9��ߔJ�$o�x�F�s<��(|~����9s��eX{U�֪��зRv�׸�߯㱜N/����{��4�M��7c���E���u4�^5dj��ۉ�_Q|(Li�(N�t�#@�� ����'9�Չb�r4CQdadb��W
�Ĺ��(7��n9�^|��aj�'�K�'�,@�jm�JԾ�C����	zS��縫�P��<Q�1.�6�(��:2=�0^����H,� �-1l]��z��9���[���ٺ��)n�(�|���5MX.�3��#Z2s8X�父xugjzu2.�	�N��S�C2о��WCV��n��;�͊U�>�m�'�E�Yy��Cހp��GN���2�d���B`��\����Fkڃ�i������m`��N����V!ǅR���m�]�\Ww�i��ivqf]��|e{�6��p�*�= 0=4���R?���������F���+�ĭ��/�� ךNV��>V���8K��C�TT�^�ݳ�t�c~q)岲Lp߮���7l����PL�mW{�(��ڦm��9��~�}l��|��
\j���	� G=\�T�F�u�x�pڞ}��>q��Į���=PV9�;T��@NkY����-�X~Ca�n�;X�����?��K��a��<��e���V���r&*e��������u5��L�E�5�j4v�11�i2�.��	x�R�!P�Y��7�ԭ�?mĥf�`]_�M4S�=,V��³����?�c�1�Ӯ�L�~g���;��_-������7�4�ҰI��:,�A�	1Cv�Ԝ���5��u䡼���P�恿�t�<��`���z\�Z�!�h5�h�咴�OY9�pE����<,;V���)\���kk5=E?�, ��j,��B�^(
��P&Y�ޥ �u�R(��Q^���@�z:�K�xa]�9PP3t!ߑE�X`Cr;2�N=�¢;zJ��wc���G?axjX1��4V����X��������]-��T�3�]&��X����b��?�#hxX�����,��7+��2���i4-Aj����1��BNy(	�5���~@��NK0�fHp���}w$*]~�2����đ�x�EQ1���"�1���6�P~�?��?՝y�e�R+;áhԛ�`v���3���Զ�ކ^e;&H�w6ee��O'ja0.�L�TFn��D7�.\Pj����{n�E�m�p�x��i��f#'E��5o�ݦ������� ��*Y&+7�pf���y���u���Z�y�?�\���.s�oS{'������Y�~X��1�,�F�{ó�p"=Kz(=�r(��_��XS�=��
\��`�u��Z2Lg��1E�);�G1-Zw_o�w���������̺TZ!�"����p�B��j�E�����+��	b��?�P4g֥0EeY��O9��o� �R\����X�z��Є�=���6Υ��Z�1)d�ix=�q��܎f_@������]��&&��S����z:�t`U�*c�P*	O�����I������D]Z4�i�j��L�5� �d��0�9I��FM���#�����<O�嘎
���A ��Pޤ�ǻgQ+:����m5�{9ӓu�Xo�W�	��9O�z?�CMX�C��ѵ���O�H�f�&�����/dw,B�
o�r���ԽP�@6F���(�2����p��S��S��+S��p:�̭�R3����	Ȍ�k�d���q��r�Y]�D�#�Ňt�󌗏vcԔY��b�,�L��N���N��΅�
� ^2��Ii�5w[��F��r���=Vn����EO'�)��sipk�LP�`m9d`5=�a������j�y�v���WX�3�3�r�Y�UV��;8�����Iڑ�1�����vc��qf��Q:�HP��e!�X�Qa�z� ����aZ �y</�/:�W-q�P�0�&�3� (��P01*�H�L^J�~&�Z�9�ˢ��ހ�bo!�6��Ʉ:�f:S�����39���s�qI�(3��d}CO�5
x��,б������+�35���?�XBF�6��ʚ�'��~���E��A��w���W�g�y�^>�H,�`�hy�B�F�$d�92��J�]Ɋo�a{<�z�v��4�����؏�t�IE���\�QQ3�`=˰��zX�:��xb4E|�Y�pU ���"A����gzW��J�:t����[���D��O�K� ���p�=��U^r�?K�9ۂ
Np��=��&��Q�rT�F��mG��p:��Z�|�P@���FDk�"��N䥠E`[� �a�+l��`7`)S�9���@/��~�Nݒq�m��K�{:�d��-�e�)���0��9� rs�e�.I]���е0�N3���)�H���Ke�szrx~�,4u�x&���1�ڎ����'�V�4H�+� �CӶrݟ�Uc��Dj7�!��|9B�;՛�[!	h/�koJ�,Z7��\и&��H�`j���V"�����3����PWSz=��Zя@��a�0���VkS�X�Y�IY��]������G���9~�qC-[Ebq��`>�*XfH$n����צ�9B�g'z��u�2�vj�x�PX\	���2{���]G��/nf�͉<ٍ�[�x�й"l�D3����f�1>)�q����u���ٛ�l䤏g���b4q�<0/�B�c.{�X��uT�K��jg\>BF8������Ef��ƾ�]�r�-e,�`�o��fG�W�W�̫ z�$N��;�SQ�O� ���"��*+�)���X�&��O�@xX�6�!��k��DuӲ�Dƍ�J\�C�&U&{��8d���y4Wb>МL��o������{Z��׽��μ��>6�jv��31b���IT]��i����ZWmdL{*x������u�8<�3�X�vϜ��+�!�(R���@���'����������L:�u���r
c�8��7���R���$�����Cȳ�ZP�`X��Y̋Ap�a����5Ug҉�����44�1B3e�Nl�c}�b2�2����c�S�؟�������vL9Ր�`������w׏qU`���qW�f ���1B�����p^��4�����G�o5?<��E0�C��g��F ������Um���X�������,������X��(����D�Eqs�U,�TA��η.���t�.O䥪��,��ܪ��ay1�	�tp�FYr�u����ϩ�ƥ��M�o'���
�����:J/����Mr`'W�Woz"/?^jo�h��X��X����1E gnAy�}�����VUm?���8{LDI����"#����`#�J���\�GW�?we�Y�0gK|�_�����������zص@�`�Q�G<2~�q�� �D�Q!}��5��^E�-#_x��+#,��0�^�_��Vun��%�f0�@�S&��՚5������h|��.m�uܬ��;|-�fO���̻t1��%��A�uN����Z���*8��+�* �fm���:&�rl5��%Oxզ�����ݭ�y�[�ϒ���%�R��2K=��I�:"I1�Uט~c<�~�sGx�O��绺c<nf5_�{�&�ㆺ�՜��X<�Z&[]�3����o�x�Y�F�����	�.�=8����&�at!�    &0hH��tW��1u��ˉ3��X����_e{��q�;)��SB��Uky����:?s�B1,f���ݒ,��L^5` x:C#�t��,k���	1͜7c\Ԙѓ��~�^��$C2��Q�c������S�<S��/�]�
k1�Qq��%a��Xk�Zr<���q�V��O���ĺ�D�R�6�F-�f�\�T�(G	�(�k}*�HY�%����Y�:�)�R�,ES���M�
 ��ޢk�:r��TKl���D^�m�r�Eb��x�E�,�&�G�u���PO�e��'�e�.:��^R�� zl�'���RB�C�z^��OG���X&�Y��R���;��'@+K/���<ټ�����e���D����N��0���d�Ā@6%��NU7�}�MinO����eB'�3�c�O��QX`Se�. ]:t9t��0I۲m����fΣ,��{մ��x�1�l�k�2*�y+-����Bmc�'r�u�\�g5&���ܐ��%��T���b�� ��T���v�1��]��by��[�x���Eq#��n<Oq*�rb� �>Mӟᣯ^fi"/����x+����ȩgn(�M�H;
W���M[�����xJ691/� ��@b����%ɭ���	�3�,`d`�8���+�3�,�S�D�^r�����Z-h����_�ώ�+�{�#����P���	�xtCae�QT�E�)T�L�^}nͶ�u��UJ��4��jr� &�_,<�\�Y���Y�_���D\tg���Im�L����S�Ji������>/��L�|�/`��m!3HFƠ=��G؀ kA�~�2��'V��(v03.�� ZoYu��Hc�C:�ԓ�J^��M~��q=��4㬣�J��Ut���y�\� 36p��������XE�k��Z�)&�e� �t,&
��y�~�?�r'�%\�2%����-/IK;Ɩ�g�i�m�k��/�xX=��}\���/�K} `+����yaV�<3${��dB�̉��eaM���j�M=����y� �5��~H�l�6Z(	 @��6:v����2u"Z�}��u�5��̗�����cW�b�	�_t
�Ŋd���n�ZW3k���}��!j��^��
�9���"�z����b�AO�h��?�s��S�Y�2���gf;I�h��?$[���X��hhJ����iLx[��v�/o��0���
/GL�3���谒��}}|�����v;7�פ.:��!�`���:-��yx��ђk;����v��
z��l3���x����A�¤I� �]m�םSܵYNv_=�������Q�n�6�rTѥ`Q��HdƷC�$@[	�vpN����2I2.����]��]&1�y��2� [^E�J@�|(������:�<���|���h�ew�~�H���ѹЊVt��_��7� ��{���ә������>�O����.O���<f�l`�����q�O �.a��w���ո�u���8��e�����D���b�Xw���o�Ś��Tx�,�y�VC�y𠲶�}t�fa�+p��W���p�`��!�00u�U�����z8�A�q��h辄��)�)8�?Od�=e�'�͉���D�=������$b�"s���v~T >���ha�����)��9۳����x�qu/L��}ͽ����s>(֖N���m�+�o����N��ǜ��Z���e���Vh���C��*�
2
�V��/����=�j���X]cu3�I)�{��%;F!s�pۓE|�� Y�=�V��u����Y�,�U�]���}ś�v�<��]�GȜ�n�M;$%��̳0#L����	��V�p�]7Y?�ﶻ]ܿ�b����7���W7�q�������p�,�FF^�c��|^8V�<���=1���������"/9�L=>^�(^�`L�̧o�h����������V�����;����۬U�;�g�zh`�"X��
fV�=s��a��'��m�(�01.AeN@�9��R0��,g\{�jy)'^]��0m�m|���Y�t��ĺ���[Rc��Z�Z0�Ѫ!�Q�H�C�M��'L1�
�CEou�R��K�]&βcO`$��H�� �Q�\f����OU�����7o��J*���ʢ%$2���f�g/�'�)�}||JS���9�13.A@*����0�I�zHI���,2#0�%�1����<�j�vf\�\Pa�9���	@ g�!0�jN���7齱��+�9�.�D82c��H��	�}�e�D�f4	c�P�?_��S��fw����̺D�E`�qLw4���w�`e)g���������M�3c�34xo����i�P0��0T����)G�Ü�z-aV�S�f�,x�|D[�M��	L�������/+c{F�o�<`�t��$��w��a��IkhQ#�R�����_$�Q�R��ZYf��=+ ��;��w�%f5T�p��DH�����
�����3؝���]�4cе
�+QYz��S�q%�� dʼ�o`�g8_�5̼˅�����=���T'�~�av�f��T��ڸ�mY���:o�ʮ��C'Ñ�ob%h�V���
�Њ��	'}���f��?nԥ�����ID����H^k#�'��Gz_��f˺����a{��b�Y������Xy��$r<��E	xN�������F�i��ʎ�� 0cx5�:1f�fYU47X���B�����*�rk�o'�rui�kd����-%=OR�RG3��b0�����鯢:'֥���HL!��=#�<=�b�c�)x��f��C{��^}���J��vK��t�M˹,�+����Xws��������Fa|�L�3I��5O<���
+1�l#�Ĕ������ӱ��W#1s޸\A��{F�a|+�yq>k�)�]F�f>��E���Q�W�KcY4,kr	C��%C�<�h���q�y�s�;��lD����Z����خ�I$�o��t^�vQ����|�ܩP)����T��ɋU9�`n�*�b2��ȓ��zF�H��#��`���v����������N�7�X��gN��ļ�8[p�)̟H���dVB��a���]ܞ�D\t'̢o�N�n%�-Di������� �����n���d�&�Rq[�K���^�[��n�d�7R��R�c��K�]gȜ�FN*k�\�W^3:^�¼ށ�͒�P��6��Wѱ�Y�ԣ����F��=s.�8���4�R�a��b�v-Ш!q��b7���X�?��Tӊo�M�!�<��+c�a�*hY^�7.`
?6�����7���ĸ�}V��E�5���a'z	��דU�.^r�<Vֶo�:��y	>R�)�V.������,�l(���g���yF�ղ>�.��Ӻ����ք��0Fn����ճ�E���
�C�Adϫ�s'����GY
�c�E�0��:��!ƨ
�2;?�rxDwGB��x��s���px��i�J.��,XɡV�w��OP5^�s��f��v�X�'��<q.G�J^�C����)�0�,����"����|wlebO�3y9�$,h(X�Ac�E��Q��)��Swo��O�:�Y96���Վy����\�?������L4,ߺ""$S5s�cU�>8 �d`�7��S�2��/������L����b��M�Ca����}��'� jAe��v��c��ZOK�'嵾���[4���G������
w*������ �\����D����f�)�1e2Ko{��ips�0T����#��n��Fk�|�/u�B����Ry^���uֻb�ja&v���W���������t�\3�RJ���3�����D����1d����4ɜ7�,7��h�O�7"6*�i��֚�Y&����F�R��*P���f�&��Q�`c(t
T-Ux�)��_�dN�[�k\��f��aF���<�=ї�+eZ{8�P�Lf��q��*�Wp� d'��'�ݜ=��|#\�~��9p������2���#p� �  �ߎ�y?�5&ɿ�\JB��h{���]	fDX��d�y�d�_�!�r�
��y'����8�#kXLЍ�p,�d��j��f�"FV��m�f��Z��6���q9���M�0�;3U�&�2!M�*�nU~�=�������~URѼ�^u �j���.<JU�9K�	��薷:3'�&`۬BS9'�R"𡖥l%�K�P�S�"qp�m"�����]m�a��OT{�Ƹ�6��$��~��'(h�W,��Xj����@���a�i��۹�řq�s��e�Wfy"��\c!JJ,g��OĉC���k�n���`#��3}�t]�6 o(�A�A&��{�?d�D^W�N-�my��˕'Τ}g����oHRLmՙI�X4}&��r��X�f�vX�Z�������bt�yC,����ف��!�Ѵ��v��v���ۉhc�跋y���n��1�/l�~�ih��M~��9��iJ�%oY�23��@�+3�eT�8/`�h����
ǆ�S��0�k楸�"E`Q�Lq���և�� <�OG iߖ�
� �nnϘ����2,��� �oW�a!��~oG7����j>� ��9P|�%a�x
*���P���#��0:�·��\̻m�Lg�)�u�,p-Ư���Y-B�8:�q����ʗ�f���m�y#/�Ȝy}|�/$��]`:�R��S�|����q��<_A���y�kR�s����b�,���5�ut�O����s;��"�'Υ���R��)��E]���8=
�y�H`"�i��'���^�&�X����Ɔ�r� \�Ü@�
v塑�͓.,,7Q7$�ե���)˰.wBy	�gE�qC'}m�|=��`�W1�+?����}
��	��J�c ��</��-~=��Q`(�����0�0�uy]~��:�n����� ��BX�.��c8��罸���9���eXHe^n9����aNn�.:;��Ǜ8���4���k�T����%]�Ë�A��Z@�ϗ�.+��6ۙ�3�-�v��T\}�0#�w,�Y����F��Q�:�C��n[�^�����g��9 �{����b;���j��������{1��8�7,h�^�O�&��)�/Q8�(��(@kk	i�Y��!����[b��Y�2ļ �כ�ϱ���������G�ΰ-X8U�Cv"�0u�0��W����4l���K�x�#{�xx�~�H�*�0���L�Ƨ���3@����-E�x+a̅��f�5B���kZ,7|��},sm/6O7���&���o�����g�BF��^���o+jj�	�й0bڹ�e���ST������ĺ9q�V�vⴶ������.K�q��N��r�/��s|u�D�7�~� c1�N׾�V�X�E�A��
<5�h�$N���7t����f��f�8o�6(��@��hy��h�	%�C>��"k̋S�����\�D��b�@]���$/|���#û�a�n��U�����b��� �k��     