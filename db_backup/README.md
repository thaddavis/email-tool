psql \
   -f bk \
   --host postgresql-marketing-contacts.camfztlkkv2f.us-east-1.rds.amazonaws.com \
   --port 5432 \
   --username postgres \
   --password \
   --dbname postgresql_marketing_contacts

psql --host postgresql-marketing-contacts.camfztlkkv2f.us-east-1.rds.amazonaws.com --port 5432 --username postgres --password --dbname postgresql_marketing_contacts


pg_dump -h postgresql-marketing-contacts.camfztlkkv2f.us-east-1.rds.amazonaws.com -p 5432 -U postgres -Fc -b -v -f ./dump.sql -j 2 -d postgresql_marketing_contacts

pg_restore -v -h 127.0.0.1 -p 5432 -U postgres -d postgresql_marketing_contacts ./dump.sql

## TIP

had to use en0 ip address as host

pg_restore -v -h 192.168.1.40 -p 5432 -U postgres -d postgresql_marketing_contacts ./dump.sql

psql \
   --host 192.168.1.40 \
   --port 5432 \
   --username postgres \
   --password \
   --dbname postgresql_marketing_contacts
