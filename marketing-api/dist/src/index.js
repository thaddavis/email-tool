"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var uuid_1 = require("uuid");
var pg_1 = require("pg");
var package_json_1 = __importDefault(require("../package.json"));
var app = express_1.default();
var port = 3000;
var pool = new pg_1.Pool({
    user: process.env.PG_USER || 'postgres',
    // host: 'postgresql-marketing-contacts.camfztlkkv2f.us-east-1.rds.amazonaws.com',
    // database: 'postgresql_marketing_contacts',
    host: process.env.PG_HOST || 'localhost',
    database: process.env.PG_DATABASE || 'marketing',
    password: process.env.PG_PASSWORD || 'password',
    port: 5432,
});
app.get('/', function (req, res) {
    res.send("Welcome to the Witherberry\u00AE Mailing List v" + package_json_1.default.version);
});
app.get('/editSubcribedStatus/:token/:value', function (req, res) {
    try {
        var editSubcribedToken = req.params.token;
        var subscribedValue_1 = req.params.value === '0' ? false : true;
        pool.query("UPDATE users \n      SET subscribed = $1,\n      editSubscribedToken = $2\n      WHERE editSubscribedToken = $3;\n    ", [subscribedValue_1, uuid_1.v4(), editSubcribedToken], function (error, results) {
            try {
                if (error)
                    throw error;
                if (results.rowCount === 1) {
                    res.status(200).send("You have successfully " + (subscribedValue_1 ? 'subscribed to' : 'unsubscribed from') + " the Witherberry\u00AE mailing list");
                }
                else {
                    throw 'Your request to update your account in the mailing was NOT successful';
                }
            }
            catch (e) {
                res.status(500).send(e.toString());
            }
        });
    }
    catch (e) {
        res.status(500).send(e.toString());
    }
});
app.listen(port, function () {
    console.log("Example app listening at http://localhost:" + port);
});
