import express, { Application, Request, Response } from 'express'
import { v4 as uuidv4 } from 'uuid';

import { Pool, QueryResult } from 'pg'


import pkg from '../package.json'

const app: Application = express()
const port:Number = Number.parseInt(process.env.PORT!) || 3000

const pool = new Pool({
  user: process.env.PG_USER || 'postgres',
  // host: 'postgresql-marketing-contacts.camfztlkkv2f.us-east-1.rds.amazonaws.com',
  // database: 'postgresql_marketing_contacts',
  host: process.env.PG_HOST || 'localhost',
  database: process.env.PG_DATABASE || 'marketing',
  password: process.env.PG_PASSWORD || 'password',
  port: 5432,
})

app.get('/marketing-api/', (req: Request, res: Response) => {
  res.send(`Welcome to the Witherberry® Mailing List v${pkg.version}`)
})

app.get('/marketing-api/editSubcribedStatus/:token/:value', (req, res) => {
  console.log('GET /marketing-api/editSubcribedStatus/:token/:value')
  console.log('process.env.PG_USER', process.env.PG_USER)
  console.log('process.env.PG_HOST', process.env.PG_HOST)
  console.log('process.env.PG_DATABASE', process.env.PG_DATABASE)
  console.log('process.env.PG_PASSWORD', process.env.PG_PASSWORD)

  try {

    const editSubcribedToken = req.params.token
    const subscribedValue = req.params.value === '0' ? false : true

    pool.query(`UPDATE users 
      SET subscribed = $1,
      editSubscribedToken = $2
      WHERE editSubscribedToken = $3;
    `, [subscribedValue, uuidv4(), editSubcribedToken], (error: Error, results: QueryResult) => {
      try {

        if (error) throw error

        if (results.rowCount === 1) {
          res.status(200).send(`You have successfully ${subscribedValue ? 'subscribed to' : 'unsubscribed from'} the Witherberry® mailing list`)
        } else {
          throw 'Your request to update your account in the mailing was NOT successful'
        }

      } catch(e) {
        res.status(500).send(e.toString())
      }
      
    })

  } catch(e) { res.status(500).send(e.toString()) }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})