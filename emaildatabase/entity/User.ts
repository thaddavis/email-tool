import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('users')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false
    })
    name: string;

    @Column()
    first_name: string;

    @Column()
    middle_name: string;

    @Column()
    last_name: string;

    @Column()
    nickname: string;

    @Column({
        unique: true
    })
    email: string;

    @Column()
    failures: string;

    @Column()
    successes: string;

    @Column({
        default: false
    })
    subscribed: boolean;
}