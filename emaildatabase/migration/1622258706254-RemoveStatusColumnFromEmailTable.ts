import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveStatusColumnFromEmailTable1622258706254 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE emails DROP COLUMN status;
            DROP TYPE email_status;
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TYPE email_status AS ENUM ('sent', 'success', 'failure');
            ALTER TABLE emails ADD COLUMN status email_status;  
        `)
    }

}
