import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUniqueConstraintToTextMessageEventsTable1626838759741 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE text_message_events
            ADD CONSTRAINT unique_recipient_id
            UNIQUE (recipient_id);
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE text_message_events
            DROP CONSTRAINT unique_recipient_id;
        `)
    }

}
