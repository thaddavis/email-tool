import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateEmailsTemplatesTable1621972746085 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          create table email_templates (
            id SERIAL PRIMARY KEY NOT NULL,
            name TEXT NOT NULL UNIQUE,
            template TEXT,
            template_arguments JSON
          );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE email_templates;
        `);
    }

}
