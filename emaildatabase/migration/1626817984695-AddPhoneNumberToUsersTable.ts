import {MigrationInterface, QueryRunner} from "typeorm";

export class AddPhoneNumberToUsersTable1626817984695 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE users ADD COLUMN phone varchar(64);
        `)
        
        
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE users DROP COLUMN phone;
        `)
    }

}
