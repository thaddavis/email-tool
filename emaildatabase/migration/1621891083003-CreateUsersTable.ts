import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateUsersTable1621891083003 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(`
          create table users (
            id SERIAL PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            first_name TEXT,
            middle_name TEXT,
            last_name TEXT,
            nickname TEXT,
            email TEXT UNIQUE,
            failures INT DEFAULT 0,
            successes INT DEFAULT 0,
            subscribed BOOL DEFAULT TRUE
          );
        `);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    
        await queryRunner.query(`
            DROP TABLE users;
        `);
    
    }

}
