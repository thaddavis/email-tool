import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTextMessageEventsTable1626819345252 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        
        await queryRunner.query(`
            create type text_message_event_type AS ENUM (
                'Sent'
            );

            create table text_message_events (
                id SERIAL PRIMARY KEY NOT NULL,
                timestamptz TIMETZ,
                recipient_id INT NOT NULL,
                text_message_event_type TEXT_MESSAGE_EVENT_TYPE NOT NULL
            );
        `)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        
        await queryRunner.query(`
            DROP TABLE text_message_events;
            DROP TYPE text_message_event_type;
        `)

    }

}
