import {MigrationInterface, QueryRunner} from "typeorm";

export class AddEditSubcribedTokenToUsersTable1627941525495 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE users ADD COLUMN editSubscribedToken varchar(64);
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE users DROP COLUMN editSubscribedToken;
        `)
    }

}
