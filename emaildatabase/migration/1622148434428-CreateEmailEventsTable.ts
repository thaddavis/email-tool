import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateEmailEventsTable1622148434428 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TYPE email_event_type AS ENUM (
                'Bounce',
                'Click',
                'Complaint',
                'Delivery',
                'Open',
                'Reject',
                'RenderingFailure',
                'Send'
            );

            create table email_events (
                id SERIAL PRIMARY KEY NOT NULL,
                timestamptz TIMETZ NOT NULL,
                event_type EMAIL_EVENT_TYPE NOT NULL,
                batch_id TEXT NOT NULL,
                ses_message_id TEXT NOT NULL
            );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE email_events;
            DROP TYPE email_event_type;
        `)

    }
}
