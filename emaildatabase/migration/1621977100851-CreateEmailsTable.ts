import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateEmailsTable1621977100851 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          CREATE TYPE email_status AS ENUM ('sent', 'success', 'failure');

          create table emails (
            id SERIAL PRIMARY KEY NOT NULL,
            timestamptz TIMETZ NOT NULL,
            recipient TEXT NOT NULL,
            email_template_id INT NOT NULL,
            status EMAIL_STATUS NOT NULL,
            batch_id TEXT NOT NULL,
            ses_message_id TEXT NOT NULL,
            CONSTRAINT fk_email_template
                FOREIGN KEY(email_template_id) 
	                REFERENCES email_templates(id)
	                ON DELETE CASCADE,
            CONSTRAINT fk_recipient
                FOREIGN KEY(recipient) 
                    REFERENCES users(email)
                    ON DELETE CASCADE
          );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE emails;
            DROP TYPE email_status;
        `);
    }

}
