import { SQSClient, ReceiveMessageCommand, DeleteMessageCommand } from "@aws-sdk/client-sqs"

import config from '../pgconfig'
const { Client } = require('pg')

const REGION = "us-east-1"

const sqsClient = new SQSClient({ region: REGION });

function pgFormatDate(date) {
    /* Via http://stackoverflow.com/questions/3605214/javascript-add-leading-zeroes-to-date */
    function zeroPad(d) {
      return ("0" + d).slice(-2)
    }
  
    var parsed = new Date(date)
  
    return [parsed.getUTCFullYear(), zeroPad(parsed.getMonth() + 1), zeroPad(parsed.getDate()), zeroPad(parsed.getHours()), zeroPad(parsed.getMinutes()), zeroPad(parsed.getSeconds())].join(" ");
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {

    const client = new Client(config)
    await client.connect()

    const command = new ReceiveMessageCommand({
        QueueUrl: 'https://sqs.us-east-1.amazonaws.com/333427308013/EmailDeliveryStatusQueue',
        AttributeNames: ['All'],
        MessageAttributeNames: ['All'],
        MaxNumberOfMessages: 10
    })

    const data = await sqsClient.send(command)

    console.log('EmailDeliveryStatusQueue data', data)
    
    if (data.Messages && data.Messages.length > 0) {
        
        console.log("message length", data.Messages.length)

        for (let m of data.Messages) {
            
            try {
            
                console.log('-*-*-')
                console.log(JSON.parse(JSON.parse(m.Body).Message))
                
                let eventType = ``
                let batchId = ``
                let timestamp = -1
                let ses_message_id = ``

                // vvv Get eventType vvv
                try {
                    console.log('! has eventType !', JSON.parse(JSON.parse(m.Body).Message).eventType)
                    eventType = JSON.parse(JSON.parse(m.Body).Message).eventType
                } catch(e) { throw e }

                // vvv Get Batch Id vvv
                try {
                    console.log('! has Batch Id !', JSON.parse(JSON.parse(m.Body).Message).mail.tags.BatchId[0])
                    batchId = JSON.parse(JSON.parse(m.Body).Message).mail.tags.BatchId[0]
                } catch(e) { throw e; }
                
                // vvv Get Timestamp vvv
                try {
                    console.log('! has Timestamp !', JSON.parse(JSON.parse(m.Body).Message).mail.timestamp)
                    timestamp = new Date(JSON.parse(JSON.parse(m.Body).Message).mail.timestamp).getTime()

                    debugger

                    console.log(' *** ', pgFormatDate(timestamp))
                    // timestamp = new Date(timestamp).toISOString();
                } catch(e) { throw e; }

                // vvv Get ses_message_id vvv
                try {
                    console.log('! has ses_message_id !', JSON.parse(JSON.parse(m.Body).Message).mail.messageId)
                    ses_message_id = JSON.parse(JSON.parse(m.Body).Message).mail.messageId
                } catch(e) { throw e }

                const text = `
                    INSERT INTO email_events(
                        timestamptz,
                        event_type,
                        batch_id,
                        ses_message_id
                    ) 
                    VALUES(to_timestamp(${timestamp}), $1, $2, $3)
                `
                const values = [
                    // timestamp,
                    eventType,
                    batchId,
                    ses_message_id
                ]

                const res = await client.query(text, values)

                console.log('save event to db', res)

                const deleteCommand = new DeleteMessageCommand({
                    QueueUrl: 'https://sqs.us-east-1.amazonaws.com/333427308013/EmailDeliveryStatusQueue',
                    ReceiptHandle: m.ReceiptHandle
                })
                const deleteCommandData = await sqsClient.send(deleteCommand)
                console.log('deleteCommandData', deleteCommandData)

            } catch(e) {
                console.log('error processing message')
            }

        }
    
    } else {
        console.log('NO MESSAGES')
    }

    await client.end()
    
    await sleep(2000)

    main()
}

main()

// var intervalFunc = setInterval(() => {
//     console.log('called')
//     main()
//     clearInterval(intervalFunc)
// }, 1000)