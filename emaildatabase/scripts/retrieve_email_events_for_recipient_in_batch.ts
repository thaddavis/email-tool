import config from '../pgconfig'
import { v4 as uuidv4 } from 'uuid';
import { stat } from 'fs';

const { Client } = require('pg')

let email_name = 'names_release_promo_email';
let batch_id = 'c43ecf6d-f054-4a96-a9f0-b5bc1e22945f';
let recipient_email = 'witherberry@gmail.com';

(async function main() {

    const client = new Client(config)
    await client.connect()

    const res = await client.query(`select id, name, template_arguments from email_templates where name='${email_name}';`)
    
    console.log('email_template with name:', res.rows)
    console.log('id of email_template with name:', res.rows[0].id)

    const email_id = res.rows[0].id
    
    const res1 = await client.query(`
        select * from emails where 
        recipient=$1 and 
        email_template_id=$2 and 
        batch_id=$3;
    `, [
        recipient_email,
        email_id,
        batch_id
    ])

    const ses_message_id = res1.rows[0].ses_message_id

    const res2 = await client.query(`
        select timestamptz, event_type from email_events where 
        ses_message_id=$1;
    `, [
        ses_message_id
    ])

    console.log('res2', res2.rows)

    // --- ___ --- ___ --- ___ ---

    await client.end()

})()