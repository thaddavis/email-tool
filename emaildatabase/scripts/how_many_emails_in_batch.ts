import config from '../pgconfig'
import { v4 as uuidv4 } from 'uuid';

const { Client } = require('pg')

let batch_id = 'ce111960-f7bf-4207-8b80-b70cbb61ae95';

(async function main() {

    const client = new Client(config)
    await client.connect()

    const res = await client.query(`select * from emails where batch_id='${batch_id}';`)
    
    console.log('amount of emails in batch:', res.rowCount)
    console.log(res.rows)

    // --- ___ --- ___ --- ___ ---

    await client.end()

})()