import {createConnection, getConnection} from "typeorm";

const parse = require('csv-parse');
const fs = require('fs');

const processFile = async () => {
    let records = []
    const parser = fs
    .createReadStream(`./contacts_csvs/contacts_5.25.21.csv`)
    // .createReadStream(`./contacts_csvs/contacts_thadduval.lavud.csv`)
    // .createReadStream(`./contacts_csvs/contacts_witherberry.csv`)
    .pipe(parse({
      columns: true
    }));
    
    for await (const record of parser) {
      records.push(record)
    }
    
    return records
}

(async function main() {

    const records: Array<{
        name: string,
        first_name: string,
        middle_name: string,
        last_name: string,
        nickname: string,
        email: string
    }> = await processFile()
    
    // console.info(records);
    // console.info(records[0])

    await createConnection()
    
    const connection = await getConnection()
    const qb = connection.createQueryBuilder()
    
    for(let u of records) {
        qb.insert().into("users")
            .values({
                name: u.name,
                first_name: u.first_name,
                middle_name: u.middle_name,
                last_name: u.last_name,
                nickname: u.nickname,
                email: u.email
            }).execute()   
    }
})()

