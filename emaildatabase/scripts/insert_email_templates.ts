import config from '../pgconfig'
const { Client } = require('pg')

const fs = require('fs');

(async function main() {


    console.log('config', config)
    const email_template_data = fs.readFileSync(__dirname + '/../email_templates/greatestfeature.html', 'utf8')
    console.log(email_template_data)
    

    const client = new Client(config)
    await client.connect()

    const text = 'INSERT INTO email_templates(name, template, template_arguments) VALUES($1, $2, $3) RETURNING *'
    const values = [
        'names_release_promo_email',
        email_template_data,
        {
            RECIPIENT_NAME: {
                type: 'string',
                description: 'name of person receiving email'
            },
            EMAIL_TITLE: {
                type: 'string',
                description: 'Most prominent message in the email body aka the title'
            },
            EMAIL_SUBTITLE: {
                type: 'string',
                description: '2nd most prominent message in the email body aka the subtitle'
            },
            CTA_TEXT: {
                type: 'string',
                description: 'Text on CTA button in email'
            },
            CTA_LINK: {
                type: 'string',
                description: 'url where email recipient will be directed'
            }
        }
    ]

    const res = await client.query(text, values)
    await client.end()

    console.log(res.rows[0])

    // console.info(records);
    // console.info(records[0])

    // await createConnection()
    
    // const connection = await getConnection()
    // const qb = connection.createQueryBuilder()

})()

