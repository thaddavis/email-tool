import { SESClient, SendTemplatedEmailCommand } from "@aws-sdk/client-ses"
import config from '../pgconfig'
import { v4 as uuidv4 } from 'uuid';

const { Client } = require('pg')

const REGION = "us-east-1"
const sesClient = new SESClient({ region: REGION })

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

(async function main() {

    const client = new Client(config)
    await client.connect()

    const res1 = await client.query("SELECT * from users;")
    
    let mailingList = res1.rows
    // let found = res1.rows.find(element => element.name === "Witherberry Entertainment");
    // mailingList.push(found)
    // let found = res1.rows.find(element => element.name === "Thad Duval");
    // mailingList.push(found)

    // --- ___ --- ___ --- ___ ---

    let batchUuid = uuidv4();

    for (let i of mailingList) {
    
        try {
            let params = {
                Destination: {
                    CcAddresses: [],
                    ToAddresses: [
                        (i.email)
                    ]
                },
                Source: 'Witherberry <admin@witherberry.net>',
                Template: 'MyTemplate2',
                TemplateData: `{
                    \"RECIPIENT_NAME\":\"${i.first_name}\",
                    \"EMAIL_TITLE\":\"NAMES THE ALBUM\",
                    \"EMAIL_SUBTITLE\":\"Enjoy!\",
                    \"SONG_TITLE\":\"NAMES THE ALBUM IS RELEASED!\",
                    \"SONG_LINK\":\"https://namesthealbum.com\",
                    \"SONG_DESCRIPTION\":\"Made with LOVE by Thadeus\",
                    \"CTA_LINK\":\"https://namesthealbum.com\", 
                    \"CTA_TEXT\":\"Listen\",
                    \"EDIT_SUBSCRIBED_STATUS_URL\":\"https://cmdsoftware.io/marketing-api/editSubcribedStatus/${i.editsubscribedtoken}/0\" 
                }`,
                // TemplateData: `{ 
                //     \"RECIPIENT_NAME\":\"${i.first_name}\",
                //     \"EMAIL_TITLE\":\"Thadeus - Names\",
                //     \"CTA_LINK\":\"https://namesthealbum.com\",
                //     \"CTA_TEXT\":\"Listen Now\",
                //     \"EMAIL_SUBTITLE\":\"Made with LOVE by Thadeus\" 
                // }`,
                ReplyToAddresses: [
                    'witherberry@gmail.com'
                ],
                ConfigurationSetName: 'Email_Delivery_Status',
                Tags: [
                    {
                        Name: "BatchId",
                        Value: batchUuid
                    }
                ]
            }

            const data = await sesClient.send(new SendTemplatedEmailCommand(params))
            // console.log('--- SendTemplatedEmailCommand data --- ', data)

            // console.log(mailingList[i])

            console.log(i.email, batchUuid, i.editsubscribedtoken)

            const res2 = await client.query(`
                INSERT INTO emails (timestamptz, recipient, email_template_id, batch_id, ses_message_id) 
                VALUES (NOW(), $1, $2, $3, $4);
            `, [
                i.email,
                1,
                batchUuid,
                data.MessageId
            ])

            console.log('___')
            
        } catch(e) {
            console.log(e)
        }

        await sleep(300);
        
    }
    
    await client.end()

})()