import config from '../pgconfig'
import { v4 as uuidv4 } from 'uuid';
import { stat } from 'fs';

const { Client } = require('pg')

let batch_id = 'ce111960-f7bf-4207-8b80-b70cbb61ae95';

// potential ses configuration set events -> Bounce, Click, Complaint, Delivery, Open, Reject, RenderingFailure, Send
let status = 'Delivery';

(async function main() {

    const client = new Client(config)
    await client.connect()

    const res = await client.query(`select * from emails where batch_id='${batch_id}';`)
    
    console.log('amount of emails in batch:', res.rowCount)
    // console.log(res.rows)

    const res1 = await client.query(`select * from email_events where batch_id='${batch_id}' and event_type='${status}';`)
    
    console.log(`amount of emails in batch with status '${status}':`, res1.rowCount)
    // console.log(res1.rows)

    console.log(`Percentage: ${ (res1.rowCount / res.rowCount * 100).toFixed(2) }%`)

    // --- ___ --- ___ --- ___ ---

    await client.end()

})()