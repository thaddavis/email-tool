import config from '../pgconfig'
import { v4 as uuidv4 } from 'uuid';

import { Client } from 'pg';

(async function main() {

    const client = new Client(config)
    await client.connect()

    const res = await client.query(`select * from users;`)
    
    console.log('users query in batch:', res.rowCount)
    console.log(res.rows)

    for (let i of res.rows) {
        console.log('i', i)

        let editSubscribedToken = uuidv4()
        let id = i.id

        const res = await client.query(`UPDATE users SET editSubscribedToken = $1 WHERE id = $2;`, [editSubscribedToken, id])
    }

    // --- ___ --- ___ --- ___ ---

    await client.end()

})()