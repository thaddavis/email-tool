"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var client_sqs_1 = require("@aws-sdk/client-sqs");
var pgconfig_1 = require("../pgconfig");
var Client = require('pg').Client;
var REGION = "us-east-1";
var sqsClient = new client_sqs_1.SQSClient({ region: REGION });
function pgFormatDate(date) {
    /* Via http://stackoverflow.com/questions/3605214/javascript-add-leading-zeroes-to-date */
    function zeroPad(d) {
        return ("0" + d).slice(-2);
    }
    var parsed = new Date(date);
    return [parsed.getUTCFullYear(), zeroPad(parsed.getMonth() + 1), zeroPad(parsed.getDate()), zeroPad(parsed.getHours()), zeroPad(parsed.getMinutes()), zeroPad(parsed.getSeconds())].join(" ");
}
function sleep(ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
}
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var client, command, data, _i, _a, m, eventType, batchId, timestamp, ses_message_id, text, values, res, deleteCommand, deleteCommandData, e_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    client = new Client(pgconfig_1.default);
                    return [4 /*yield*/, client.connect()];
                case 1:
                    _b.sent();
                    command = new client_sqs_1.ReceiveMessageCommand({
                        QueueUrl: 'https://sqs.us-east-1.amazonaws.com/333427308013/EmailDeliveryStatusQueue',
                        AttributeNames: ['All'],
                        MessageAttributeNames: ['All'],
                        MaxNumberOfMessages: 10
                    });
                    return [4 /*yield*/, sqsClient.send(command)];
                case 2:
                    data = _b.sent();
                    console.log('EmailDeliveryStatusQueue data', data);
                    if (!(data.Messages && data.Messages.length > 0)) return [3 /*break*/, 10];
                    console.log("message length", data.Messages.length);
                    _i = 0, _a = data.Messages;
                    _b.label = 3;
                case 3:
                    if (!(_i < _a.length)) return [3 /*break*/, 9];
                    m = _a[_i];
                    _b.label = 4;
                case 4:
                    _b.trys.push([4, 7, , 8]);
                    console.log('-*-*-');
                    console.log(JSON.parse(JSON.parse(m.Body).Message));
                    eventType = "";
                    batchId = "";
                    timestamp = -1;
                    ses_message_id = "";
                    // vvv Get eventType vvv
                    try {
                        console.log('! has eventType !', JSON.parse(JSON.parse(m.Body).Message).eventType);
                        eventType = JSON.parse(JSON.parse(m.Body).Message).eventType;
                    }
                    catch (e) {
                        throw e;
                    }
                    // vvv Get Batch Id vvv
                    try {
                        console.log('! has Batch Id !', JSON.parse(JSON.parse(m.Body).Message).mail.tags.BatchId[0]);
                        batchId = JSON.parse(JSON.parse(m.Body).Message).mail.tags.BatchId[0];
                    }
                    catch (e) {
                        throw e;
                    }
                    // vvv Get Timestamp vvv
                    try {
                        console.log('! has Timestamp !', JSON.parse(JSON.parse(m.Body).Message).mail.timestamp);
                        timestamp = new Date(JSON.parse(JSON.parse(m.Body).Message).mail.timestamp).getTime();
                        debugger;
                        console.log(' *** ', pgFormatDate(timestamp));
                        // timestamp = new Date(timestamp).toISOString();
                    }
                    catch (e) {
                        throw e;
                    }
                    // vvv Get ses_message_id vvv
                    try {
                        console.log('! has ses_message_id !', JSON.parse(JSON.parse(m.Body).Message).mail.messageId);
                        ses_message_id = JSON.parse(JSON.parse(m.Body).Message).mail.messageId;
                    }
                    catch (e) {
                        throw e;
                    }
                    text = "\n                    INSERT INTO email_events(\n                        timestamptz,\n                        event_type,\n                        batch_id,\n                        ses_message_id\n                    ) \n                    VALUES(to_timestamp(" + timestamp + "), $1, $2, $3)\n                ";
                    values = [
                        // timestamp,
                        eventType,
                        batchId,
                        ses_message_id
                    ];
                    return [4 /*yield*/, client.query(text, values)];
                case 5:
                    res = _b.sent();
                    console.log('save event to db', res);
                    deleteCommand = new client_sqs_1.DeleteMessageCommand({
                        QueueUrl: 'https://sqs.us-east-1.amazonaws.com/333427308013/EmailDeliveryStatusQueue',
                        ReceiptHandle: m.ReceiptHandle
                    });
                    return [4 /*yield*/, sqsClient.send(deleteCommand)];
                case 6:
                    deleteCommandData = _b.sent();
                    console.log('deleteCommandData', deleteCommandData);
                    return [3 /*break*/, 8];
                case 7:
                    e_1 = _b.sent();
                    console.log('error processing message');
                    return [3 /*break*/, 8];
                case 8:
                    _i++;
                    return [3 /*break*/, 3];
                case 9: return [3 /*break*/, 11];
                case 10:
                    console.log('NO MESSAGES');
                    _b.label = 11;
                case 11: return [4 /*yield*/, client.end()];
                case 12:
                    _b.sent();
                    return [4 /*yield*/, sleep(2000)];
                case 13:
                    _b.sent();
                    main();
                    return [2 /*return*/];
            }
        });
    });
}
main();
// var intervalFunc = setInterval(() => {
//     console.log('called')
//     main()
//     clearInterval(intervalFunc)
// }, 1000)
//# sourceMappingURL=read_sqs_events_queue.js.map