"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var client_ses_1 = require("@aws-sdk/client-ses");
var pgconfig_1 = require("../pgconfig");
var uuid_1 = require("uuid");
var Client = require('pg').Client;
var REGION = "us-east-1";
var sesClient = new client_ses_1.SESClient({ region: REGION });
(function main() {
    return __awaiter(this, void 0, void 0, function () {
        var client, res1, mailingList, found, batchUuid, _i, mailingList_1, i, params, data, res2, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    client = new Client(pgconfig_1.default);
                    return [4 /*yield*/, client.connect()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, client.query("SELECT * from users;")
                        // console.log('res1.rows', res1.rows)
                    ];
                case 2:
                    res1 = _a.sent();
                    mailingList = [];
                    found = res1.rows.find(function (element) { return element.name === "Witherberry Entertainment"; });
                    mailingList.push(found);
                    found = res1.rows.find(function (element) { return element.name === "Thad Duval"; });
                    mailingList.push(found);
                    batchUuid = uuid_1.v4();
                    _i = 0, mailingList_1 = mailingList;
                    _a.label = 3;
                case 3:
                    if (!(_i < mailingList_1.length)) return [3 /*break*/, 9];
                    i = mailingList_1[_i];
                    _a.label = 4;
                case 4:
                    _a.trys.push([4, 7, , 8]);
                    params = {
                        Destination: {
                            CcAddresses: [],
                            ToAddresses: [
                                (i.email)
                            ]
                        },
                        Source: 'Witherberry <admin@witherberry.net>',
                        Template: 'MyTemplate2',
                        TemplateData: "{ \n                    \"RECIPIENT_NAME\":\"" + i.first_name + "\",\n                    \"EMAIL_TITLE\":\"Thadeus - Names\",\n                    \"CTA_LINK\":\"https://namesthealbum.com\",\n                    \"CTA_TEXT\":\"Listen Now\",\n                    \"EMAIL_SUBTITLE\":\"Made with LOVE by Thadeus\" \n                }",
                        ReplyToAddresses: [
                            'witherberry@gmail.com'
                        ],
                        ConfigurationSetName: 'Email_Delivery_Status',
                        Tags: [
                            {
                                Name: "BatchId",
                                Value: batchUuid
                            }
                        ]
                    };
                    return [4 /*yield*/, sesClient.send(new client_ses_1.SendTemplatedEmailCommand(params))];
                case 5:
                    data = _a.sent();
                    console.log('--- SendTemplatedEmailCommand data --- ', data);
                    // console.log(mailingList[i])
                    console.log(i.email, batchUuid);
                    return [4 /*yield*/, client.query("\n                INSERT INTO emails (timestamptz, recipient, email_template_id, batch_id, ses_message_id) \n                VALUES (NOW(), $1, $2, $3, $4);\n            ", [
                            i.email,
                            1,
                            batchUuid,
                            data.MessageId
                        ])];
                case 6:
                    res2 = _a.sent();
                    console.log('___');
                    return [3 /*break*/, 8];
                case 7:
                    e_1 = _a.sent();
                    console.log(e_1);
                    return [3 /*break*/, 8];
                case 8:
                    _i++;
                    return [3 /*break*/, 3];
                case 9: return [4 /*yield*/, client.end()];
                case 10:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
})();
//# sourceMappingURL=send_email.js.map